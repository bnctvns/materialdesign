package com.bonioctavianus.materialdesign.activity;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import com.bonioctavianus.materialdesign.R;
import com.bonioctavianus.materialdesign.adapter.TabAdapter;
import com.bonioctavianus.materialdesign.myinterface.RecyclerViewListener;

/**
 * Created by bonioctavianus on 1/20/16.
 */
public class MainActivity extends BaseActivity implements RecyclerViewListener {
    public static final String TAG = MainActivity.class.getSimpleName();

    private TabAdapter mTabAdapter;
    private ViewPager mPager;
    private TabLayout mTabLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate() called");

        initView();
        initToolbar();
        initDrawer();
        initNavigationViewListener();
        setNavigationMenuCheckedItem(R.id.navigation_item_1);
        initViewPager();
    }

    private void initView() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavigationView = (NavigationView) findViewById(R.id.navigation_drawer);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        mPager = (ViewPager) findViewById(R.id.view_pager);
        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
    }

    public void initViewPager() {
        mTabAdapter = new TabAdapter(getSupportFragmentManager());
        mPager.setAdapter(mTabAdapter);
        mTabLayout.setTabsFromPagerAdapter(mTabAdapter);
        mTabLayout.setupWithViewPager(mPager);
        mTabLayout.getTabAt(0).setText(R.string.tab_1);
        mTabLayout.getTabAt(1).setText(R.string.tab_2);
        mTabLayout.getTabAt(2).setText(R.string.tab_3);

        mPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(
                mTabLayout));
    }
}