package com.bonioctavianus.materialdesign.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.bonioctavianus.materialdesign.fragment.FragmentA;
import com.bonioctavianus.materialdesign.fragment.FragmentB;
import com.bonioctavianus.materialdesign.fragment.FragmentC;

/**
 * Created by bonioctavianus on 1/20/16.
 */
public class TabAdapter extends FragmentStatePagerAdapter {

    public TabAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FragmentA();
            case 1:
                return new FragmentB();
            case 2:
                return new FragmentC();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}