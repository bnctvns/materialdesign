package com.bonioctavianus.materialdesign.fragment;

import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.bonioctavianus.materialdesign.myinterface.RecyclerViewListener;

/**
 * Created by bonioctavianus on 1/20/16.
 */
public class BaseFragment extends Fragment {
    public static final String TAG = BaseFragment.class.getSimpleName();

    public static RecyclerViewListener mFragListener;
    public SwipeRefreshLayout mSwipeRefreshLayout;
    public RecyclerView mRecyclerView;
    public Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

        try {
            mFragListener = (RecyclerViewListener) context;

        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement RecyclerViewListener interface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mFragListener = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate() called");
    }
}